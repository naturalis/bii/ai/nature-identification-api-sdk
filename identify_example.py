import argparse
import json

from api.calls import call_identify
from api.shared import API_URL
from tools.visual import draw_regions


def main():
    api_response = call_identify([args.images], api_url=args.api_url)
    print_response = True  # TODO: make CLI argument
    if print_response:
        print(json.dumps(api_response, indent=2))
    visualize = True
    if visualize:  # TODO: make CLI argument
        draw_regions(api_response, images=[args.images])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("images", help="TODO")  # TODO: make it accept multiple images
    parser.add_argument("--api_url", help="TODO", default=API_URL)

    args = parser.parse_args()

    main()
