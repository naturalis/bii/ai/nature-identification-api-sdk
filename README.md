```
python identify.py test_data/keizer.jpg --api_url https://full-scale-test-multisource.demo.naturalis.io/v1/observation/identify?autozoom_enabled=1
```

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

- `pre-commit autoupdate`
- `pre-commit install`
