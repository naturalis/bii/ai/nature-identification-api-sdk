import os
from cv2 import cv2
from typing import List


def draw_regions(j_response, images: List[os.PathLike]):
    # multi_out_folder = os.path.join(output_folder, "object_images")
    # os.makedirs(multi_out_folder, exist_ok=True)
    region_map = {}  # {{r["id"]: r for r in j_response["regions"]}}
    region_group_map = {}
    for region_group in j_response["region_groups"]:
        region_group_map[region_group["id"]] = region_group
        for region in region_group["regions"]:
            region_map[region["id"]] = region
    image_map = {r["id"]: r for r in j_response["media"]}
    for image_path in images:
        frame = cv2.imread(image_path)
        for region_prediction in j_response["predictions"]:
            region_group = region_group_map[region_prediction["region_group_id"]]
            for region in region_group["regions"]:
                height, width = frame.shape[:2]
                color = (
                    51,
                    223,
                    255,
                )
                geometry = region["box"]
                j_image = image_map[region["media_id"]]
                # if j_image["filename"] != image.uid: # TODO:
                #     continue
                x1 = int(geometry["x1"] * width)
                y1 = int(geometry["y1"] * height)
                x2 = int(geometry["x2"] * width)
                y2 = int(geometry["y2"] * height)
                cv2.rectangle(frame, (x1, y1), (x2, y2), color, 5)
                max_prediction = region_prediction["taxa"]["items"][0]
                class_ = max_prediction["scientific_name"]
                class_prob = max_prediction["probability"]
                box_label = f"{class_} ({class_prob:.2f}) - {region_group['id']}"
                is_full = "full" in region["id"]
                draw_text(
                    width / 1000.0,
                    frame,
                    box_label,
                    x1,
                    min(height, y2) if not is_full else y1,
                    20 if is_full else 0,
                    color,
                    x2=x2,
                    label_offset=20 if is_full else -20,
                )
        # out_path = os.path.join(
        #     multi_out_folder, f"{record_id}_{image.uid}.jpg"
        # ).replace(":", "_NS_")
        # print(out_path)
        # cv2.imwrite(out_path, frame)
        cv2.namedWindow("foo", cv2.WINDOW_NORMAL)
        cv2.setWindowProperty("foo", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        cv2.imshow("foo", frame)
        # Waits for a keystroke
        cv2.waitKey(0)


def draw_text(
    font_scale,
    frame,
    text,
    x1,
    y1,
    y_offset,
    color=(255, 255, 255),
    x2: int = None,
    label_offset: int = None,
):
    """
    Draw text onto a given frame with the cv2 library
    :param font_scale: scale of font hershey plain
    :param frame: frame to draw text on
    :param text: text to draw
    :param x1: absolute x position in frame
    :param y1: absolute y position in frame
    :param y_offset: y offset in frame
    """
    y_offset = int(y_offset * font_scale)
    label_offset = int(label_offset * font_scale)
    if x2 is not None:
        cv2.rectangle(
            frame,
            (x1, y1),
            (x2, y1 + label_offset),
            color=(255, 255, 255),
            thickness=-1,
        )
        color = (0, 0, 0)
    cv2.putText(
        frame,
        text,
        (x1, y1 + y_offset),
        cv2.FONT_HERSHEY_PLAIN,
        font_scale,
        color,
        int(font_scale),
    )
