import argparse
import json

import requests

from api.shared import API_BASE_URL


def get_api_version(api_url):
    response = requests.get(api_url).json()
    print(json.dumps(response, indent=2))
    print("VERSION TAG:", response["tag"])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--api_url", help="TODO", default=API_BASE_URL + "/v1/observation/documentation"
    )
    args = parser.parse_args()
    get_api_version(args.api_url)
