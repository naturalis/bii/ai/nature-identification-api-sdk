import glob
import json
import os
from pathlib import Path

from api.calls import call_identify
from tools.visual import draw_regions

API_URL = (
    "http://192.168.1.218:5004/v1/observation/identify?autozoom_enabled=1&autozoom_debug_save_zooms=1"
    "&autozoom_global_threshold=0.5&autozoom_multi_species=1"
)


def m(test_data_folder):

    for image_path in glob.glob(str(test_data_folder / "*")):
        print(image_path)
        api_response = call_identify([image_path], api_url=API_URL)
        print_response = True  # TODO: make CLI argument
        if print_response:
            print(json.dumps(api_response, indent=2))
        visualize = True
        if visualize:  # TODO: make CLI argument
            draw_regions(api_response, images=[image_path])


if __name__ == "__main__":
    m(test_data_folder=Path(os.getcwd()) / ".." / "test_data")
