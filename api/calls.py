import os

import requests
from requests.auth import HTTPBasicAuth
from typing import List, Union


def call_identify(
    images: List[os.PathLike],
    api_url:str,
    api_user=None,
    api_password=None,
    force_submodel: str = None,
):
    files = []
    for image_path in images:
        files.append(("image", open(image_path, "rb")))

    try:
        completed_url = (
            api_url
            + (("&" if "?" in api_url else "?") + f"force_submodel={force_submodel}")
            if force_submodel is not None
            else api_url
        )
        request = requests.post(
            completed_url,
            files=files,
            auth=HTTPBasicAuth(api_user, api_password)
            if api_user is not None
            else None,
            timeout=(5, 60),
        )
    except:
        # TODO:
        raise
        pass

    return request.json()
